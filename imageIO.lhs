
Supplied in this file:

  * Input/Output Functions
  * Functions to perform conversion to/from PPM
  * Datatypes concerning images
  * A small sample SizedImage

Don't alter any of this code; instead, add your own functions at the
end of the supplied code.



> module Main where
> import System.IO
> import System.IO.Error

Input/Output Functions
**********************

The file IO functions are based on some functions in the book 
"The Haskell School of Expression", by Paul Hudak.
You do not need to understand them!!!! All you need to know is that
to run the program, you type the function

              transform f

where f is whatever function you wish to perform on the input image, of type

              f :: SizedImage -> SizedImage

It asks for an input file and an output file, and then performs
the function transformImageFile on the contents. 

> transform f = do fromHandle <- getAndOpenFile "PPM input file:" ReadMode
>                  toHandle   <- getAndOpenFile "PPM output file:" WriteMode
>                  contents   <- hGetContents fromHandle
>                  hPutStr toHandle ((transformImageFile f) contents)
>                  hClose fromHandle
>                  hClose toHandle
>                  putStrLn "Done."
          
          
> getAndOpenFile :: String -> IOMode -> IO Handle
> getAndOpenFile prompt mode 
>       = do putStr prompt
>            name <- getLine
>            catchIOError (do handle <- openFile name mode
>                             return handle)
>                         (\error -> do putStrLn ("Cannot open " ++ name)
>                                       print error
>                                       getAndOpenFile prompt mode)
 


Manipulating an image file involves 
1) Extracting the image (& its size) out of the PPM file
2) Transforming the image in some way, according to the function f
3) Converting the transformed image back into PPM format
So we have

> transformImageFile :: (SizedImage -> SizedImage) -> String -> String
> transformImageFile f = convertToPPM . f . extractFromPPM
 
 
 
 
transform2 is an alternative function which writes the output PPM file to the
screen rather than an output file, which can be easier to see what's
going on if you want to look directly at the pixel values.
To run the program, you type the function

              transform2 f

where f is whatever function you wish to perform on the input image, of type

              f :: SizedImage -> SizedImage
              
> transform2 f = do fromHandle <- getAndOpenFile "PPM input file:" ReadMode
>                   contents   <- hGetContents fromHandle
>                   putStrLn ((transformImageFile f) contents)
>                   hClose fromHandle
>                   putStrLn "Done."

analyse is another IO function, for when you want to display some information
about the image on the screen. 
To run the program, you type the function

              analyse f

where f is whatever function you wish to perform on the input image, of type

              f :: SizedImage -> a 
              
where a is some type displayable on the screen by using the function "show"
           
> analyse f = do fromHandle <- getAndOpenFile "PPM input file:" ReadMode
>                contents   <- hGetContents fromHandle
>                putStrLn ((show . f . extractFromPPM) contents)
>                hClose fromHandle
>                putStrLn "Done."
           
Functions to perform conversion to/from PPM
*******************************************

Don't alter the functions in this section. 
You don't need to understand these functions, either!!


The following functions extract a sized image from the contents of a PPM file:

> extractFromPPM :: String -> SizedImage
> extractFromPPM contents 
>   = (w,h,pix)
>   where xss = lines contents
>         (w,h,pixstr) = getWidthHeight (drop 2 xss)
>         pix = triples (getNumbers pixstr)

The job of the getWidthHeight function is to get the listed width
and height of the image, listed on the first line, and return these
two numbers along with the rest of the lines containing the pixel information.

> getWidthHeight (line:otherlines)
>           = (width,height,unlines' (tail otherlines))
>           where width  = head wh
>                 height = head (tail wh)
>                 wh = getNumbers line

unlines' is much like the standard function unlines, but puts space
characters between the lines, not newline characters.

> unlines' [] = []
> unlines' (x:xs) = x ++ " " ++ unlines' xs

The job of getNumbers is to convert a string (representing a list of numbers
separated by spaces) into the list of numbers (Int integers) it represents.

> getNumbers :: String -> [Int]
> getNumbers str 
>    | xs==[] = []
>    | otherwise = n : getNumbers theRest
>    where xs          = reads str
>          (n,theRest) = head xs
                    
triples takes the list of numbers and converts them to pixels.

> triples :: [Int] -> PixelData
> triples (r:g:b:is) = (r,g,b): triples is
> triples        is  = []




The following functions convert a SizedImage to a PPM 

> convertToPPM :: SizedImage -> String
> convertToPPM image = "P3\n" 
>                      ++ "# generated PPM file \n"
>                      ++ showImage image

showImage lists the image data in the format required by the 
PPM file format

> showImage :: SizedImage -> String
> showImage (width,height,pixels) 
>         = show width ++ " " ++ show height ++ "\n" 
>           ++ "255\n" ++ showData pixels

showData lists all the pixel data

> showData :: PixelData -> String 
> showData = unlines . (map showAsChars) . groupByLine

groupByLine breaks the list of pixels up into lists of 7, so that
in the final file, 7 pixels are listed per line

> groupByLine :: [Pixel] -> [[Pixel]]
> groupByLine []     = []
> groupByLine (p:ps) = (take perLine (p:ps)): groupByLine (drop perLine (p:ps))
>                      where perLine = 7

showAsChars takes a list of pixels and produces a string listing the rgb
values of the pixels one after the other, separated by spaces

> showAsChars :: [Pixel] -> String
> showAsChars []         = ""
> showAsChars [p]        = showPixel p 
> showAsChars (p1:p2:ps) = showPixel p1 ++ " " ++ showAsChars (p2:ps) 

showPixel shows the pixel as a string with the red green and blue values,
separated by space characters

> showPixel :: Pixel -> String
> showPixel (r,g,b) = show r ++ " " ++ show g ++ " " ++ show b



Datatypes concerning images
***************************

Each pixel is represented by its colour, given as RGB values (red,green,blue)

> type Pixel      = (Int, Int, Int)

The pixel data for an image is just a list of pixels

> type PixelData  = [Pixel]


A SizedImage consists of the width of the image, its height, and its pixel data.

> type SizedImage = (Int,Int,PixelData)


A small sample image
********************

> sampleImage :: SizedImage
> sampleImage = (4,3, [(0,0,0), (  0,  0,  0), (  0,  0,  0), (255,  0,  0)]
>                 ++  [(0,0,0), (255,255,255), (255,  0,  0), (150,150,255)]
>                 ++  [(0,0,0), (255,  0,  0), (150,150,255), (150,150,255)])


Your own code
*************

To start you off, here's a transformation that does 
absolutely nothing to the image (just returns the original image):

> noChange :: SizedImage -> SizedImage
> noChange image = image

You can test this by typing  "transform noChange" at the Haskell prompt.

Image Parameter Getters
***********************

> width :: SizedImage -> Int
> width (w, h, pd) = w

> height :: SizedImage -> Int
> height (w, h, pd) = h
 
> pixelData :: SizedImage -> PixelData
> pixelData (w, h, pd) = pd

Pixel Parameter Getters
***********************

> redValue :: Pixel -> Int
> redValue (r, g, b) = r

> greenValue :: Pixel -> Int
> greenValue (r, g, b) = g

> blueValue :: Pixel -> Int
> blueValue (r, g, b) = b

Per Pixel Operations
********************

> mapImage :: (a -> a) -> (Int,Int,[a]) -> (Int, Int, [a])
> mapImage f (w, h, pd) = (w, h, map f pd)

Make Grey
*********

> avgPixel :: Pixel -> Int
> avgPixel (r,g,b) = div (r + g + b) 3

> makeGrey :: Pixel -> Pixel
> makeGrey (r, g, b) = (avg, avg, avg)
>   where avg = avgPixel (r,g,b)

> greyscale :: SizedImage -> SizedImage
> greyscale img = mapImage makeGrey img

Invert
******

> invertPixel :: Pixel -> Pixel
> invertPixel (r, g, b) = (255 - r, 255 - g, 255 - b)

> invert :: SizedImage -> SizedImage
> invert img = mapImage invertPixel img

Clamp (Restrict a pixel r,g,b to the range 0-255)
*************************************************

> clamp :: Pixel -> Pixel
> clamp (r, g, b) = (min 255 (max 0 r), min 255 (max 0 g), min 255 (max 0 b))

Darken (divide pixel values by two?)
************************************

> darken :: SizedImage -> SizedImage
> darken img = mapImage (\(r, g, b) -> (r `div` 2, g `div` 2, b `div` 2)) img

Colour Filter
*************

> multPixel :: Pixel-> Pixel -> Pixel
> multPixel (ma, mb, mc) (pa, pb, pc) = (ma * pa, mb * pb, mc * pc)

> redComponent :: SizedImage -> SizedImage
> redComponent img = mapImage (multPixel (1, 0, 0)) img

> greenComponent :: SizedImage -> SizedImage
> greenComponent img = mapImage (multPixel (0, 1, 0)) img

> blueComponent :: SizedImage -> SizedImage
> blueComponent img = mapImage (multPixel (0, 0, 1)) img


Average
*******

> averagePixels :: SizedImage -> Int
> averagePixels (w, h, pd) = (sum (map avgPixel pd)) `div` (w * h)

Treshold
********

> thresholdPixel :: Int -> Pixel -> Pixel
> thresholdPixel thresh pix
>	| avgPixel pix > thresh = (255, 255, 255)
>	| otherwise = (0, 0, 0)

> threshold :: Int -> SizedImage -> SizedImage
> threshold thresh img = mapImage (thresholdPixel thresh) img

Convert image to list of rows
*****************************

> makeSplit :: Int -> [a] -> ([a],[a])
> makeSplit n a = ((take n a), (drop n a))

> getRows :: Int -> [a] -> [[a]]
> getRows _ [] = []
> getRows n xs = as : (getRows n) bs 
> 	where (as,bs) = makeSplit n xs

> type SizedRowImage = (Int,Int,[[Pixel]])

> convertToRows :: SizedImage -> SizedRowImage
> convertToRows (w, h, pd) = (w, h, getRows w pd)

> convertFromRows :: SizedRowImage -> SizedImage
> convertFromRows (w, h, pd) = (w, h, foldl (++) [] pd)

> transposePixels :: [[Pixel]] -> [[Pixel]]
> transposePixels ([]:_) = []
> transposePixels x = (map head x) : transposePixels (map tail x)

> transposeImage :: SizedRowImage -> SizedRowImage
> transposeImage (w, h, pd) = (h, w, transposePixels pd)

> flipHorizontally :: SizedImage -> SizedImage
> flipHorizontally = convertFromRows.(mapImage reverse).convertToRows

> flipVertically :: SizedImage -> SizedImage
> flipVertically img= convertFromRows (w, h, reverse pd)
> 				where (w, h, pd) = convertToRows img

> upsideDown :: SizedImage -> SizedImage
> upsideDown (w, h, pd) = (w, h, reverse pd)

> rotateLeft :: SizedImage -> SizedImage
> rotateLeft = flipVertically.convertFromRows.transposeImage.convertToRows

> rotateRight :: SizedImage -> SizedImage
> rotateRight = flipHorizontally.convertFromRows.transposeImage.convertToRows

Section B
*********

Crop The Image
*****************

> cropRows :: Int -> Int -> Int -> Int -> SizedRowImage -> [[Pixel]]
> cropRows x y w h (iw, ih, rows) 
>	| x > iw = []
>	| y > ih = []
>	| h <= 0 = []
>	| w <= 0 = []
>	| y > 0 = cropRows x 0 w h (iw, ih, drop y rows)
>	| otherwise = ((take w).(drop x).head) rows : (cropRows x 0 w (h-1) (iw, ih, drop 1 rows))

> cropImage :: Int -> Int -> Int -> Int -> SizedImage -> SizedImage
> cropImage x y w h img = convertFromRows (w, h, cropRows x y w h (convertToRows img))

> zipPixel :: (Int -> Int -> Int) -> Pixel -> Pixel -> Pixel
> zipPixel f (aa, ab, ac) (ba, bb, bc) = (aa `f` ba, ab `f` bb, ac `f` bc)

> mapPixel :: (Int -> Int) -> Pixel -> Pixel
> mapPixel f (a, b, c) = (f a, f b, f c)

Image Convolution
*****************

** Larger sample images

> rowSampleImg :: SizedRowImage
> rowSampleImg = convertToRows sampleImage

> smallSampleImage :: SizedImage
> smallSampleImage = (3, 3, [(0,0,0), (0,0,0), (0,0,0), (0,0,0), (255,255,255), (0,0,0), (0,0,0), (0,0,0), (0,0,0)])

blur smallSampleImage = (1,1,[(28, 28, 28)])

> otherSampleImage :: SizedImage
> otherSampleImage = (8,7, [(255,255,255), (0,0,0), (0,0,0), (0,0,0), (0,0,0), (0,0,0), (0,0,0)]
>                      ++  [(0,0,0), (255,255,255), (0,0,0), (0,0,0), (0,0,0), (0,0,0), (0,0,0)]
>                      ++  [(0,0,0), (0,0,0), (255,255,255), (0,0,0), (0,0,0), (0,0,0), (0,0,0)]
>                      ++  [(0,0,0), (0,0,0), (0,0,0), (255,255,255), (0,0,0), (0,0,0), (0,0,0)]
>                      ++  [(0,0,0), (0,0,0), (0,0,0), (0,0,0), (255,255,255), (0,0,0), (0,0,0)]
>                      ++  [(0,0,0), (0,0,0), (0,0,0), (0,0,0), (0,0,0), (255,255,255), (0,0,0)]
>                      ++  [(0,0,0), (0,0,0), (0,0,0), (0,0,0), (0,0,0), (0,0,0), (255,255,255)]
>                      ++  [(0,0,0), (0,0,0), (0,0,0), (0,0,0), (0,0,0), (0,0,0), (0,0,0)])

blur otherSampleImage 
                    (6,6, [(85,85,85),(56,56,56),(28,28,28),(0,0,0),(0,0,0),(0,0,0),
                           (56,56,56),(85,85,85),(56,56,56),(28,28,28),(0,0,0),(0,0,0),
                           (28,28,28),(56,56,56),(85,85,85),(56,56,56),(28,28,28),(0,0,0),
                           (0,0,0),(28,28,28),(56,56,56),(85,85,85),(56,56,56),(28,28,28),
                           (0,0,0),(0,0,0),(28,28,28),(56,56,56),(85,85,85),(56,56,56),
                           (0,0,0),(0,0,0),(0,0,0),(28,28,28),(56,56,56),(85,85,85)])

> otherRowSampleImg :: SizedRowImage
> otherRowSampleImg = convertToRows otherSampleImage

Take only the rows of a sizedRowImage

> rows :: SizedRowImage -> [[Pixel]]
> rows (_, _, pd) = pd

Take the cartesian product of {0..w-1} and {0..h-1}
eachCoordinate 3 3 = [(0,0), (0,1), (0,2), (1, 0), ... (2, 2)]

> eachCoordinate :: Int -> Int -> Int -> Int -> [(Int, Int)]
> eachCoordinate a b w h = [(x,y) | y <- [b..h-1], x <- [a..w-1]] -- Cartesian Product of the coordinates

Crop a (w, h) sized image around the center (x, y)

> cropAround :: (Int, Int) -> (Int, Int) -> SizedImage -> SizedImage
> cropAround (x, y) (w, h) img = cropImage (x - div w 2) (y - div h 2) w h img

Convert the image to a SizedImage from a SizedRowImage, apply transformation f and convert the result to a SizedRowImage

> transformAsRows :: (SizedImage -> SizedImage) -> SizedRowImage -> SizedRowImage
> transformAsRows f img = (convertToRows.f.convertFromRows) img

All Zeroes Pixel

> zeroPixel :: Pixel
> zeroPixel = (0, 0, 0)

Multiply the patch[x,y] with convolution[x, y]

> multiplyPatchPixel :: [[Pixel]] -> [[Pixel]] -> (Int, Int) -> Pixel
> multiplyPatchPixel patch kernel (x, y) = zipPixel (*) (patch !! y !! x) (kernel !! y !! x)

multiplyPatchPixel (rows rowSampleImg) (rows rowSampleImg) (1, 1) = 
(255,255,255) * (255,255,255) = (65025, 65025, 65025)

Multiply all the pixels in the patch with their corresponding convolution pixels giving a flat array of pixels

> multiplyPatch :: Int -> Int -> [[Pixel]] -> [[Pixel]] -> [Pixel]
> multiplyPatch w h patch kernel = map (multiplyPatchPixel patch kernel) (eachCoordinate 0 0 w h)

multiplyPatch 3 3  (rows rowSampleImg) (rows rowSampleImg) = 
 [(0, 0, 0), (0, 0, 0), ( 0, 0, 0), (0, 0, 0), (65025, 65025, 65025), (65025, 0, 0), (0, 0, 0), (65025, 0, 0), (22500, 22500, 65025)]

Sum all the pixels in the patch multiplied with corresponding convolution values.

> --                     Image           Kernel        X      Y     Result
> convolutePixel :: SizedRowImage -> SizedRowImage -> (Int, Int) -> Pixel
> -- Multiply each pixel in the patch by the corresponding convolution matrix pixel
> convolutePixel img (w, h, kernel) (x, y) = 
> 	foldr (zipPixel (+)) zeroPixel (multiplyPatch w h patch kernel)
>             where (nw, nh, patch) = transformAsRows (cropAround (x, y) (w, h)) img

transformAsRows (cropAround (1, 1) (3, 3)) rowSampleImg  
(3,3,[[(0,0,0),(0,0,0),(0,0,0)],[(0,0,0),(255,255,255),(255,0,0)],[(0,0,0),(255,0,0),(150,150,255)]])

 [Note. You would scale the values of the final image after the whole convolution is applied]

> --                      Kernel        Source Img         Result
> convoluteRowImage :: SizedRowImage -> SizedRowImage -> SizedImage
> convoluteRowImage kernel (w, h, src) = (w-2, h-2, map (convolutePixel (w, h, src) kernel) (eachCoordinate 1 1 (w-1) (h-1)))

Convolute the image by a kernel
Out(x, y) = Σi(Σj(Image(x - i)))
https://developer.apple.com/library/ios/documentation/Performance/Conceptual/vImage/Art/kernel_convolution.jpg

> convoluteImage :: SizedRowImage -> SizedImage -> SizedImage
> convoluteImage kernel img = convoluteRowImage kernel (convertToRows img)

[Note that for the sample image you will get only 2 pixels, because of the small size of the image]

> minPixelValue, maxPixelValue :: Pixel -> Int
> maxPixelValue (r,g,b) = maximum [r,g,b]
> minPixelValue (r,g,b) = minimum [r,g,b]

 Normalise Image
*****************

     x - min
  ------------- * max
    max - min

> normaliseValue :: Int -> Int -> Int -> Int
> normaliseValue low high val = floor ((((fromIntegral val) - (fromIntegral low)) / ((fromIntegral high) - (fromIntegral low))) * 255.0)

> normalisePixelValue :: Int -> Int -> Pixel -> Pixel
> normalisePixelValue low high pixel = mapPixel (normaliseValue low high) pixel

> maxImageValue, minImageValue :: [Pixel] -> Int
> maxImageValue pixels = (maximum.map (maxPixelValue)) pixels
> minImageValue pixels = (minimum.map (minPixelValue)) pixels

> normaliseImage :: SizedImage -> SizedImage
> normaliseImage (w, h, pd) = (w, h, map (normalisePixelValue minPx maxPx) pd)
>    where
>         minPx = minImageValue pd
>         maxPx = maxImageValue pd

 Convolute then normalise the image
************************************

> convoluteNormalised :: SizedRowImage -> SizedImage -> SizedImage
> convoluteNormalised kernel = (normaliseImage.(convoluteImage kernel))

 Convolute then make an average out of the sums (/9)
*****************************************************

> convoluteAveraged :: SizedRowImage -> SizedImage -> SizedImage
> convoluteAveraged (kw, kh, kernel) image = (w, h, [mapPixel (`div` 9) x | x <- img])
>    where
>         (w, h, img) = convoluteImage (kw, kh, kernel) image
>         kernelArea = (kw *  kh)


 Stock Convolution Kernels
***************************

> sobelKernel :: SizedRowImage
> sobelKernel = (3, 3, [[(-1, -1, -1), (0, 0, 0), (1, 1, 1)], [(-2, -2, -2), (0, 0, 0), (2, 2, 2)], [(-1, -1, -1), (0, 0, 0), (1, 1, 1)]])

> sobel :: SizedImage -> SizedImage
> sobel = (convoluteNormalised sobelKernel)

> embossKernel :: SizedRowImage
> embossKernel = (3, 3, [[(-2, -2, -2), (-2, -2, -2), (0, 0, 0)], [(-2, -2, -2), (6, 6, 6), (0, 0, 0)], [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])

> emboss :: SizedImage -> SizedImage
> emboss = (convoluteNormalised embossKernel)

> sharpenKernel :: SizedRowImage
> sharpenKernel = (3, 3, [[(0, 0, 0), (-1, -1, -1), (0, 0, 0)], [(-1, -1, -1), (5, 5, 5), (-1, -1, -1)], [(0, 0, 0), (-1, -1, -1), (0, 0, 0)]])

> sharpen :: SizedImage -> SizedImage
> sharpen = (convoluteNormalised sharpenKernel)

> blurKernel :: SizedRowImage
> blurKernel = (3, 3, [[(1, 1, 1), (1, 1, 1), (1, 1, 1)], [(1, 1, 1), (1,1,1), (1, 1, 1)], [(1, 1, 1), (1, 1, 1), (1, 1, 1)]])

> blur :: SizedImage -> SizedImage
> blur = (convoluteAveraged blurKernel)

Kernel that does nothing to an image (will effectivley crop out from a 1 pixel border)

> noChangeKernel :: SizedRowImage
> noChangeKernel = (3, 3, [[(0, 0, 0), (0, 0, 0), (0, 0, 0)], [(0, 0, 0), (1, 1, 1), (0, 0, 0)], [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])

