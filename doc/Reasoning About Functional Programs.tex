%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%  ********************************************

\documentclass[12pt]{article}
\usepackage[total={6.5in,8.75in},
top=1.2in, left=0.9in, includefoot]{geometry}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{lipsum}
\usepackage{xargs} % More than one opt' param in new commands
\usepackage[pdftex,dvipsnames]{xcolor} % Coloured text

\usepackage[colorinlistoftodos,prependcaption,textsize=tiny]{todonotes}
\usepackage{minted}
\renewcommand{\listingscaption}{Code Listing}

\begin{document}
\begin{titlepage}

% Defines a new command for the horizontal lines
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

% Center everything on the page
\center
 
% Heading
%%%%%%%%%%

\begin{figure}
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{brookes_logo.pdf_tex}
\end{figure}
\textsc{BSc Computer Science}\\[0.5cm]

% Title
%*******

\HRule \\[0.4cm]
\title{Reasoning About Functional Programs}
{ \huge \bfseries U08074 --- Reasoning about Functional Programs}\\[0.4cm]
\HRule \\[1.5cm]

% Author
%********
\begin{minipage}{0.4\textwidth}
\begin{flushleft} \large
\emph{Author:}\\
Oliver \textsc{Paraskos} % Your name
\end{flushleft}
\end{minipage}
~
\begin{minipage}{0.4\textwidth}
\begin{flushright} \large
\emph{Module Leader:} \\
Dr. Clare \textsc{Martin} % Supervisor's Name
\end{flushright}
\end{minipage}\\[2cm]

% Date
%******

{\large \today}\\[2cm]

% Fill the rest of the page with whitespace
\vfill
\end{titlepage}

% ----------------------------------------------------
\section{Section A}

Section A consists of some small exercises based around predefined types representing images:

\begin{listing}[H]
\begin{minted}{haskell}
type SizedImage  = (Int,Int,PixelData)
type PixelData   = [Pixel]
type Pixel       = (Int,Int,Int)
\end{minted}
\caption{Image Types.}
\end{listing}

\subsection{Exercise 1}

This first exercise asked for a definition of the following signatures for the functions width, height and pixelData.

\begin{listing}[H]
\begin{minted}{haskell}
width :: SizedImage -> Int
height :: SizedImage -> Int
pixelData :: SizedImage -> [Pixel]
\end{minted}
\caption{Exercise 1.}
\end{listing}

\begin{listing}[H]
\begin{minted}{haskell}
width :: SizedImage -> Int
width (w, h, pd) = w

height :: SizedImage -> Int
height (w, h, pd) = h
 
pixelData :: SizedImage -> PixelData
pixelData (w, h, pd) = pd
\end{minted}
\caption{Solutions for Exercise 1.}
\end{listing}

\subsection{Exercise 2}

The second exercise is to define functions, which given an input pixel will return the red, green, and blue values respectively.

\begin{listing}[H]
\begin{minted}{haskell}
redValue :: Pixel -> Int
greenValue :: Pixel -> Int
blueValue :: Pixel -> Int
\end{minted}
\caption{Exercise 2.}
\end{listing}

\begin{minted}{haskell}
\end{minted}
\begin{listing}[H]
\begin{minted}{haskell}
redValue :: Pixel -> Int
redValue (r, g, b) = r

greenValue :: Pixel -> Int
greenValue (r, g, b) = g

blueValue :: Pixel -> Int
blueValue (r, g, b) = b
\end{minted}
\caption{Solutions for Exercise 2.}
\end{listing}

\subsection{Exercise 3}

Exercise 3 is to write a function called makeGrey with the given type. This function is to take as input a single pixel and assigns to each RGB value the average of the RGB values. 

Using the makeGrey function, supply the definition of a function  called greyscale with the given type which turns an image from colour into shades of grey by applying makeGrey to each pixel

\begin{listing}[H]
\begin{minted}{haskell}
makeGrey :: Pixel -> Pixel
greyscale :: SizedImage -> SizedImage
\end{minted}
\caption{Exercise 3.}
\end{listing}

To complete this task two extra functions mapImage and avgPixel have been added which will also be used later, This helps to avoid code duplication. mapImage applies a function $f$ to each pixel of an image. avgPixel takes a single pixel and returns the average of each component.


\begin{listing}[H]
\begin{minted}[mathescape]{haskell}

-- mapImage applies $f(x)$ to all pixels in the image
mapImage :: (a -> a) -> (Int,Int,[a]) -> (Int, Int, [a])
mapImage f (w, h, pd) = (w, h, map f pd)

-- $\sum_{i=1}^{3}P_i \div 3$
avgPixel :: Pixel -> Int
avgPixel (r,g,b) = div (r + g + b) 3

-- Give each component the average of all the components
makeGrey :: Pixel -> Pixel
makeGrey (r, g, b) = (avg, avg, avg)
  where avg = avgPixel (r,g,b)

-- Convert an image to greyscale
greyscale :: SizedImage -> SizedImage
greyscale img = mapImage makeGrey img
\end{minted}
\caption{Solutions for Exercise 3.}
\end{listing}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{greyscale.pdf_tex}
    \caption{Gandalf the greyscale.}
\end{figure}

\subsection{Exercise 4}
Exercise four asks for a function invertPixel matching the given type which takes a pixel and assigns to each RGB value its "opposite" value, which is $255 - original~value$ (e.g. 0 is the opposite of 255, 253 is the opposite of 2, etc.).

Then, using the invertPixel function, supply the definition of a function invert with the given type, such that it turns an input image into its negative or inverse.

Included is a test case that applies the invert function twice,  the output file is the same as the input as can be seen.

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
invertPixel :: Pixel -> Pixel
invert :: SizedImage -> SizedImage
\end{minted}
\caption{Exercise 4.}
\end{listing}

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
invertPixel :: Pixel -> Pixel
invertPixel (r, g, b) = (255 - r, 255 - g, 255 - b)

invert :: SizedImage -> SizedImage
invert img = mapImage invertPixel img
\end{minted}
\caption{Solutions for Exercise 4.}
\end{listing}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{invert.pdf_tex}
    \caption{output of "transform (invert)", showing that the invert function is an involution}
\end{figure}

\subsection{Exercise 5}

Write a function darken to darken an image

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
darken :: SizedImage -> SizedImage
\end{minted}
\caption{Exercise 5.}
\end{listing}

This implementation of the darken function simply divides each channel by two. This means that the values will never go out of the 0--255 range.

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
darken :: SizedImage -> SizedImage
darken img = mapImage (\(r, g, b) -> (r `div` 2, g `div` 2, b `div` 2)) img
\end{minted}
\caption{Solutions for Exercise 5.}
\end{listing}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{darken.pdf_tex}
    \caption{Show that the function darkens an image}
\end{figure}

\subsection{Exercise 6}

Implement a function redComponent which displays just the red component of an image

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
redComponent :: SizedImage -> SizedImage
\end{minted}
\caption{Exercise 6.}
\end{listing}

This function was implemented generically by adding a function multPixel to multiply two pixels. This can be used as a component mask by using only 1 on r, g, or b exclusively and 0 everywhere else. For completeness greenComponent and blueComponent have also been included.

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
-- Multiply two pixels entry-wise $(A \circ B)_{ij} = A_{ij} \cdot B_{ij}$
multPixel :: Pixel-> Pixel -> Pixel
multPixel (ma, mb, mc) (pa, pb, pc) = (ma * pa, mb * pb, mc * pc)

redComponent :: SizedImage -> SizedImage
redComponent img = mapImage (maskPixel (1, 0, 0)) img

greenComponent :: SizedImage -> SizedImage
greenComponent img = mapImage (maskPixel (0, 1, 0)) img

blueComponent :: SizedImage -> SizedImage
blueComponent img = mapImage (maskPixel (0, 0, 1)) img
\end{minted}
\caption{Solutions for Exercise 6.}
\end{listing}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{redComponent.pdf_tex}
    \caption{output of "transform (redComponent)"}
\end{figure}

\subsection{Exercise 7}

Implement a function averagePixels which calculates the average brightness of all the pixels in an image. The brightness of one pixel is the average of its RGB values.

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
averagePixels :: SizedImage -> Int
\end{minted}
\caption{Exercise 7.}
\end{listing}

The implementation of averagePixels uses the avgPixel function described earlier which calculates the intensity of a single pixel as the mean of its components.

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
-- avgPixel function from earlier
--   $\sum_{i=1}^{3}P_i \div 3$
avgPixel :: Pixel -> Int
avgPixel (r,g,b) = div (r + g + b) 3

-- ...
-- averagePixels sums the results of avgPixel 
--   then divides the result by the number of pixels
averagePixels :: SizedImage -> Int
averagePixels (w, h, pd) = (sum (map avgPixel pd)) `div` (w * h)
\end{minted}
\caption{Solutions for Exercise 7.}
\end{listing}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{averagePixels.pdf_tex}
    \caption{the results of "analyse (averagePixels)"}
\end{figure}

The image used for testing is a 4x4 matrix consisting of as many blue pixels as white, the white pixels are (255,255,255) the blue pixels are (71,97,130). We know there are as many white pixels as blue pixels so the result should be the average of the averages of both pixels. Below shows manual working and the result aligns with the result obtained by the averagePixels function. 

\begin{align*}
avgPixel ((255, 255, 255)) &= \frac{255 + 255 + 255}{3} &= 255 \\
avgPixel ((71, 97, 130)) &= \frac{71 + 97 + 130}{3} &= 99\frac{1}{3}\\ 
\\
averagePixels(Image) &= \frac{255 + 99\frac{1}{3}}{2} &= 177\frac{1}{6}\\
\end{align*}


\subsection{Exercise 8}

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
threshold :: Int -> SizedImage -> SizedImage
\end{minted}
\caption{Exercise 8.}
\end{listing}

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
thresholdPixel :: Int -> Pixel -> Pixel
thresholdPixel thresh pix
    | avgPixel pix > thresh = (255, 255, 255)
    | otherwise = (0, 0, 0)

threshold :: Int -> SizedImage -> SizedImage
threshold thresh img = mapImage (thresholdPixel thresh) img
\end{minted}
\caption{Solutions for Exercise 8.}
\end{listing}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{threshold140.pdf_tex}
    \caption{results of "transform (threshold 140)"}
\end{figure}

\subsection{Exercise 9}

Define a function makeSplit using the given type definition which splits up a list into its first n elements (or fewer if there are less than n elements in the list) and the rest of the elements.

Then use the makeSplit function to define a getRows function which transforms a list into a list of sublists of length w (the last list might have to be shorter).


\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
makeSplit :: Int -> [a] -> ([a],[a])

-- For Example,
-- makeSplit 5 [1..14]  => ([1,2,3,4,5],[6,7,8,9,10,11,12,13,14])

getRows :: Int -> [a] -> [[a]]

-- For example,
-- getRows 5 [1..14]  => [[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14]]
\end{minted}
\caption{Exercise 9.}
\end{listing}


\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
makeSplit :: Int -> [a] -> ([a],[a])
makeSplit n a = ((take n a), (drop n a))

getRows :: Int -> [a] -> [[a]]
getRows _ [] = []
getRows n xs = as : (getRows n) bs 
	where (as,bs) = makeSplit n xs
    
\end{minted}
\caption{Solutions for Exercise 9.}
\end{listing}

Output from testing within console shows that the function is correct for at least this test case.


\begin{figure}[H]
\centering
\def\svgwidth{0.5\columnwidth}
\begin{minted}{bash}
*Main> getRows 5 [1..14]
[[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14]]
\end{minted}
\caption{Get a list of rows from an image in ghci}
\end{figure}


\subsection{Exercise 10}

Define a type SizedRowImage as given to be used in further exercises. Then implement two conversion functions to convert back and forth between a SizedImage and its SizedRowImage equivalent using the type definitions given below.


\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
type SizedRowImage = (Int,Int,[[Pixel]])  

convertToRows :: SizedImage -> SizedRowImage
convertFromRows :: SizedRowImage -> SizedImage  
\end{minted}
\caption{Exercise 10}
\end{listing}

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
convertToRows :: SizedImage -> SizedRowImage
convertToRows (w, h, pd) = (w, h, makeSplitEvery w pd)

convertFromRows :: SizedRowImage -> SizedImage
convertFromRows (w, h, pd) = (w, h, foldl (++) [] pd)
\end{minted}
\caption{Solutions for Exercise 10}
\end{listing}

\begin{figure}[H]
\centering
\def\svgwidth{0.5\columnwidth}
\begin{minted}{bash}
*Main> convertToRows sampleImage 
(4,3,[[(0,0,0),(0,0,0),(0,0,0),(255,0,0)],
      [(0,0,0),(255,255,255),(255,0,0),(150,150,255)],
      [(0,0,0),(255,0,0),(150,150,255),(150,150,255)]])

*Main> (convertFromRows.convertToRows) sampleImage 
(4,3,[(0,0,0),(0,0,0),(0,0,0),(255,0,0),
      (0,0,0),(255,255,255),(255,0,0),(150,150,255),
      (0,0,0),(255,0,0),(150,150,255),(150,150,255)])
\end{minted}
\caption{Converting \emph{to} and \emph{from} Rows in ghci}
\end{figure}

\subsection{Exercise 11}
Finally for this section implement the functions for the type definitions given.
 

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
upsideDown :: SizedImage -> SizedImage
flipVertically :: SizedImage -> SizedImage
flipHorizontally :: SizedImage -> SizedImage
rotateLeft :: SizedImage -> SizedImage
rotateRight :: SizedImage -> SizedImage
\end{minted}
\caption{Exercise 11.}
\end{listing}

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
flipHorizontally :: SizedImage -> SizedImage
flipHorizontally = convertFromRows.(mapImage reverse).convertToRows

flipVertically :: SizedImage -> SizedImage
flipVertically img= convertFromRows (w, h, reverse pd)
    where (w, h, pd) = convertToRows img

upsideDown :: SizedImage -> SizedImage
upsideDown (w, h, pd) = (w, h, reverse pd)
\end{minted}
\caption{Solutions for Exercise 11.}
\end{listing}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{upsideDown.pdf_tex}
    \caption{output of "transform (upsideDown)"}
\end{figure}
\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{flipVertically.pdf_tex}
    \caption{output of "transform (flipVertically)"}
\end{figure}
\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{flipHorizontally.pdf_tex}
    \caption{output of "transform (flipHorizontally)"}
\end{figure}

Rotating the image left or right can be done in terms of a transposition of the matrix (flipping the matrix along the main diagonal) and then a flip as defined already.

The transposition was implemented in the same way as \emph{http://stackoverflow.com/q/2578930} the Data.List.transpose could also be used but the top of the says \emph{"Don't alter any of this code; instead, add your own functions at the end of the supplied code."}. Import statements aren't allowed anywhere else in the lhs file.

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
-- Transposing the matrix by taking the head 
--    of all the rows as each new row 
--    resulting in the transposed $M^T$
transposePixels :: [[Pixel]] -> [[Pixel]]
transposePixels ([]:_) = []
transposePixels x = (map head x) : transposePixels (map tail x)
-- Transpose a whole image (h and w will be swapped)
transposeImage :: SizedRowImage -> SizedRowImage
transposeImage (w, h, pd) = (h, w, transposePixels pd)
-- Rotating left is a transpose then a flip vertically
rotateLeft :: SizedImage -> SizedImage
rotateLeft = flipVertically.convertFromRows.transposeImage.convertToRows
-- Rotating right is a transpose then a flip horizontally
rotateRight :: SizedImage -> SizedImage
rotateRight = flipHorizontally.convertFromRows.transposeImage.convertToRows

\end{minted}
\caption{Solutions for Exercise 11 (cont).}
\end{listing}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{rotateLeft.pdf_tex}
    \caption{output of "transform (rotateLeft)"}
\end{figure}
\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{rotateRight.pdf_tex}
    \caption{output of "transform (rotateRight)"}
\end{figure}

\section{Section B}

Implement between two and four functions that transform images in an interesting way. For Example;
    Increase the brightness; reduce or increase the contrast; enlarge the image; reduce the image in size; produce some statistics about how many of which colour pixel there are in the image; stretch the image horizontally or vertically...
Alternatively implement other ideas for image transformations.

\subsection{Crop}
Pick a smaller subset of the image, cropImage is a wrapper around cropRows, a recursive function that will drop the unneeded rows.

cropAround crops using a given point as a center point rather than the top left corner of the target image.


\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
cropRows :: Int -> Int -> Int -> Int -> SizedRowImage -> [[Pixel]]
cropImage :: Int -> Int -> Int -> Int -> SizedImage -> SizedImage
cropAround :: (Int, Int) -> (Int, Int) -> SizedImage -> SizedImage
\end{minted}
\caption{Type definitions for the crop functions.}
\end{listing}

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
cropRows :: Int -> Int -> Int -> Int -> SizedRowImage -> [[Pixel]]
cropRows x y w h (iw, ih, rows) 
    | x > iw = []
    | y > ih = []
    | h <= 0 = []
    | w <= 0 = []
    | y > 0 = cropRows x 0 w h (iw, ih, drop y rows)
    | otherwise = ((take w).(drop x).head) rows : 
        (cropRows x 0 w (h-1) (iw, ih, drop 1 rows))

cropImage :: Int -> Int -> Int -> Int -> SizedImage -> SizedImage
cropImage x y w h img = convertFromRows (w, h, cropRows x y w h (convertToRows img))

cropAround :: (Int, Int) -> (Int, Int) -> SizedImage -> SizedImage
cropAround (x, y) (w, h) img = cropImage (x - div w 2) (y - div h 2) w h img
\end{minted}
\caption{Cropping Source Code.}
\end{listing}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{cropImage.pdf_tex}
    \caption{output of "transform (cropImage 316 17 93 107)"}
\end{figure}

\subsection{Normalise}
Adjust the range of values in the image such that it best fits its histogram, for example an image that is washed out or over exposed can be bought back to a normal image this does not increase the precision of the image however leading to blank spaces in the histogram. 

\begin{minted}[mathescape]{haskell}
minPixelValue, maxPixelValue :: Pixel -> Int
maxPixelValue (r,g,b) = maximum [r,g,b]
minPixelValue (r,g,b) = minimum [r,g,b]
-- $\frac{x - min}{max - min} \times max$
normaliseValue :: Int -> Int -> Int -> Int
normaliseValue low high val = floor ((((fromIntegral val) - (fromIntegral low)) /
    ((fromIntegral high) - (fromIntegral low))) *
    255.0)

normalisePixelValue :: Int -> Int -> Pixel -> Pixel
normalisePixelValue low high pixel = mapPixel (normaliseValue low high) pixel

maxImageValue, minImageValue :: [Pixel] -> Int
maxImageValue pixels = (maximum.map (maxPixelValue)) pixels
minImageValue pixels = (minimum.map (minPixelValue)) pixels

normaliseImage :: SizedImage -> SizedImage
normaliseImage (w, h, pd) = (w, h, map (normalisePixelValue minPx maxPx) pd)
    where
        minPx = minImageValue pd
        maxPx = maxImageValue pd

\end{minted}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{normalise.pdf_tex}
    \caption{output of "transform (normalise)" on spock and the previously darkened image}
\end{figure}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{normalise_2.pdf_tex}
    \caption{histogram of "transform (normalise)" on spock}
\end{figure}

\subsection{Convolution}
Apply a convolution kernel of size 3x3 to the image discarding outermost pixels (though other convolution algorithms may extend, wrap or consider boundary pixels to be a given colour allowing an output of the same size). The result can be averaged or normalised which may have different effects depending on the convolution kernel. Other implementations of this may allow kernels larger than 3x3 however the code presented here makes an assumption that the kernels' size is exactly 3x3. 

Starting with the main function of the convolution operation, convoluteRowImage converts an image to a SizedRowImage, then applies the kernel using convoluteRowImage, defined later. In effect convoluteImage is a fa\c{c}ade meaning that the user does not have to convert to a row image first.

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
-- Facade around convoluteRowImage
convoluteImage :: SizedRowImage -> SizedImage -> SizedImage
convoluteImage kernel img = convoluteRowImage kernel (convertToRows img)
\end{minted}
\caption{The main function of the convolution operation.}
\end{listing}

Since another function acts as a façade to convoluteRowImage the output type does not have to match the input type, purely as a matter of convenience. the convoluteRowImage function iterates over each coordinate of the image calling convolutePixel for each coordinate.

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
-- eachCoordinate 3 3 = [(0,0), (0,1), (0,2), (1, 0), ... (2, 2)]
eachCoordinate :: Int -> Int -> Int -> Int -> [(Int, Int)]
eachCoordinate a b w h = [(x,y) | y <- [b..h-1], x <- [a..w-1]]
-- Convolute a SizedRowImage producing a SizedImage
convoluteRowImage :: SizedRowImage -> SizedRowImage -> SizedImage
convoluteRowImage kernel (w, h, src) = 
    (w-2, h-2, map 
        (convolutePixel (w, h, src) kernel) 
        (eachCoordinate 1 1 (w-1) (h-1)))
\end{minted}
\caption{Implementation of the convolution operation.}
\end{listing}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\columnwidth]{kernel_convolution.jpg}
    \caption{Illustration of convolution from the apple iOS developer pages.}
\end{figure}

convolutePixel applies the kernel to a patch (blue in the diagram) surrounding the pixel at the given coordinates (red in the diagram) and returns the result as the output / destination pixel. To do this convolutePixel first creates a patch by cropping around the desired pixel, it then calls multiplyPatch and sums all the results to produce the output value for this pixel.

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
-- Starting value for a fold
zeroPixel :: Pixel
zeroPixel = (0, 0, 0)
-- Multiply each pixel in the patch by the corresponding convolution matrix pixel
convolutePixel :: SizedRowImage -> SizedRowImage -> (Int, Int) -> Pixel
convolutePixel img (w, h, kernel) (x, y) = 
    foldr (zipPixel (+)) zeroPixel (multiplyPatch w h patch kernel)
        where (nw, nh, patch) = transformAsRows (cropAround (x, y) (w, h)) img
\end{minted}
\caption{Implementation of the convolution operation.}
\end{listing}

The multiplication is broken down into two stages multiplyPatch which iterates over each pixel in the patch calling multiplyPatchPixel which multiplies the patch pixel with its corresponding kernel pixel.


\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
-- Multiply a single pixel in the patch with it corresponding kernel pixel
multiplyPatchPixel :: [[Pixel]] -> [[Pixel]] -> (Int, Int) -> Pixel
multiplyPatchPixel patch kernel (x, y) = zipPixel (*)
    (patch !! y !! x) 
    (kernel !! y !! x)
-- Multiply all the pixels in the patch with their corresponding convolution pixels giving a -- flat array of pixels
multiplyPatch :: Int -> Int -> [[Pixel]] -> [[Pixel]] -> [Pixel]
multiplyPatch w h patch kernel = map
     (multiplyPatchPixel patch kernel) 
     (eachCoordinate 0 0 w h)
\end{minted}
\caption{Generate a list $x_{ij} = patch_{ij} \times kernel_{ij}$}
\end{listing}

The convolution is made useful either by taking an average of the multiplied values in a patch or by normalising the image afterwards. For example a blur would want to take the average, whereas a sobel (edge-detection) would want to normalise the image if it were to be displayed to the user (normally sobel's results are used within a more complex image processing application so there's often no need to remain in the 0-255 range)

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
-- Convolute then normalise the image
convoluteNormalised :: SizedRowImage -> SizedImage -> SizedImage
convoluteNormalised kernel = (normaliseImage.(convoluteImage kernel))
-- Convolute then make an average out of the sums (/9)
convoluteAveraged :: SizedRowImage -> SizedImage -> SizedImage
convoluteAveraged (kw, kh, kernel) image = (w, h, [mapPixel (`div` 9) x | x <- img])
   where
        (w, h, img) = convoluteImage (kw, kh, kernel) image
        kernelArea = (kw *  kh)
\end{minted}
\caption{convolute averaged or normalised}
\end{listing}

Included are some example kernels, but there are many more for different purposes

\begin{listing}[H]
\begin{minted}[mathescape]{haskell}
sobelKernel :: SizedRowImage
sobelKernel = (3, 3, [[(-1, -1, -1), (0, 0, 0), (1, 1, 1)], 
                      [(-2, -2, -2), (0, 0, 0), (2, 2, 2)],
                      [(-1, -1, -1), (0, 0, 0), (1, 1, 1)]])

sobel :: SizedImage -> SizedImage
sobel = (convoluteNormalised sobelKernel)

embossKernel :: SizedRowImage
embossKernel = (3, 3, [[(-2, -2, -2), (-2, -2, -2), (0, 0, 0)], 
                       [(-2, -2, -2), (6, 6, 6), (0, 0, 0)],
                       [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])

emboss :: SizedImage -> SizedImage
emboss = (convoluteNormalised embossKernel)

sharpenKernel :: SizedRowImage
sharpenKernel = (3, 3, [[(0, 0, 0), (-1, -1, -1), (0, 0, 0)], 
                        [(-1, -1, -1), (5, 5, 5), (-1, -1, -1)], 
                        [(0, 0, 0), (-1, -1, -1), (0, 0, 0)]])

sharpen :: SizedImage -> SizedImage
sharpen = (convoluteNormalised sharpenKernel)

blurKernel :: SizedRowImage
blurKernel = (3, 3, [[(1, 1, 1), (1, 1, 1), (1, 1, 1)], 
                     [(1, 1, 1), (1,1,1), (1, 1, 1)], 
                     [(1, 1, 1), (1, 1, 1), (1, 1, 1)]])

blur :: SizedImage -> SizedImage
blur = (convoluteAveraged blurKernel)
-- Kernel that does nothing to an image (will effectively remove a 1 pixel border)
noChangeKernel :: SizedRowImage
noChangeKernel = (3, 3, [[(0, 0, 0), (0, 0, 0), (0, 0, 0)],
                         [(0, 0, 0), (1, 1, 1), (0, 0, 0)],
                         [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])
\end{minted}
\caption{Example Convolution Kernels}
\end{listing}

\begin{figure}[H]
    \centering
    \def\svgwidth{0.5\columnwidth}
    \input{convolution.pdf_tex}
    \caption{output of "transform(emboss)" and "transform(blur)" respectivley }
\end{figure}

\section{Appendix A: imageIO.lhs}

The listing below is a representation of the code submitted along with this pdf. However some parts have been removed for brevity (some comments) and indentation has been changed but not checked to prevent code running off the end of the page.

\begin{minted}[mathescape]{haskell}

Image Parameter Getters
***********************

> width :: SizedImage -> Int
> width (w, h, pd) = w

> height :: SizedImage -> Int
> height (w, h, pd) = h
 
> pixelData :: SizedImage -> PixelData
> pixelData (w, h, pd) = pd

Pixel Parameter Getters
***********************

> redValue :: Pixel -> Int
> redValue (r, g, b) = r

> greenValue :: Pixel -> Int
> greenValue (r, g, b) = g

> blueValue :: Pixel -> Int
> blueValue (r, g, b) = b

Per Pixel Operations
********************

> mapImage :: (a -> a) -> (Int,Int,[a]) -> (Int, Int, [a])
> mapImage f (w, h, pd) = (w, h, map f pd)

Make Grey
*********

> avgPixel :: Pixel -> Int
> avgPixel (r,g,b) = div (r + g + b) 3

> makeGrey :: Pixel -> Pixel
> makeGrey (r, g, b) = (avg, avg, avg)
>   where avg = avgPixel (r,g,b)

> greyscale :: SizedImage -> SizedImage
> greyscale img = mapImage makeGrey img

Invert
******

> invertPixel :: Pixel -> Pixel
> invertPixel (r, g, b) = (255 - r, 255 - g, 255 - b)

> invert :: SizedImage -> SizedImage
> invert img = mapImage invertPixel img

Clamp (Restrict a pixel r,g,b to the range 0-255)
*************************************************

> clamp :: Pixel -> Pixel
> clamp (r, g, b) = (min 255 (max 0 r), min 255 (max 0 g), min 255 (max 0 b))

Darken (divide pixel values by two?)
************************************

> darken :: SizedImage -> SizedImage
> darken img = mapImage (\(r, g, b) -> (r `div` 2, g `div` 2, b `div` 2)) img


Colour Filter
*************

> multPixel :: Pixel-> Pixel -> Pixel
> multPixel (ma, mb, mc) (pa, pb, pc) = (ma * pa, mb * pb, mc * pc)

> redComponent :: SizedImage -> SizedImage
> redComponent img = mapImage (multPixel (1, 0, 0)) img

> greenComponent :: SizedImage -> SizedImage
> greenComponent img = mapImage (multPixel (0, 1, 0)) img

> blueComponent :: SizedImage -> SizedImage
> blueComponent img = mapImage (multPixel (0, 0, 1)) img

Average
*******

> averagePixels :: SizedImage -> Int
> averagePixels (w, h, pd) = (sum (map avgPixel pd)) `div` (w * h)

Treshold
********

> thresholdPixel :: Int -> Pixel -> Pixel
> thresholdPixel thresh pix
>	| avgPixel pix > thresh = (255, 255, 255)
>	| otherwise = (0, 0, 0)

> threshold :: Int -> SizedImage -> SizedImage
> threshold thresh img = mapImage (thresholdPixel thresh) img

Convert image to list of rows
*****************************

> makeSplit :: Int -> [a] -> ([a],[a])
> makeSplit n a = ((take n a), (drop n a))

> getRows :: Int -> [a] -> [[a]]
> getRows _ [] = []
> getRows n xs = as : (getRows n) bs 
> 	where (as,bs) = makeSplit n xs

> type SizedRowImage = (Int,Int,[[Pixel]])

> convertToRows :: SizedImage -> SizedRowImage
> convertToRows (w, h, pd) = (w, h, getRows w pd)

> convertFromRows :: SizedRowImage -> SizedImage
> convertFromRows (w, h, pd) = (w, h, foldl (++) [] pd)

> transposePixels :: [[Pixel]] -> [[Pixel]]
> transposePixels ([]:_) = []
> transposePixels x = (map head x) : transposePixels (map tail x)

> transposeImage :: SizedRowImage -> SizedRowImage
> transposeImage (w, h, pd) = (h, w, transposePixels pd)

> flipHorizontally :: SizedImage -> SizedImage
> flipHorizontally = convertFromRows.(mapImage reverse).convertToRows

> flipVertically :: SizedImage -> SizedImage
> flipVertically img= convertFromRows (w, h, reverse pd)
> 				where (w, h, pd) = convertToRows img

> upsideDown :: SizedImage -> SizedImage
> upsideDown (w, h, pd) = (w, h, reverse pd)

> rotateLeft :: SizedImage -> SizedImage
> rotateLeft = flipVertically.convertFromRows.transposeImage.convertToRows

> rotateRight :: SizedImage -> SizedImage
> rotateRight = flipHorizontally.convertFromRows.transposeImage.convertToRows

Section B
*********

Crop The Image
*****************

> cropRows :: Int -> Int -> Int -> Int -> SizedRowImage -> [[Pixel]]
> cropRows x y w h (iw, ih, rows) 
>	| x > iw = []
>	| y > ih = []
>	| h <= 0 = []
>	| w <= 0 = []
>	| y > 0 = cropRows x 0 w h (iw, ih, drop y rows)
>	| otherwise = ((take w).(drop x).head) rows :
>        (cropRows x 0 w (h-1) (iw, ih, drop 1 rows))

> cropImage :: Int -> Int -> Int -> Int -> SizedImage -> SizedImage
> cropImage x y w h img = convertFromRows (w, h, cropRows x y w h (convertToRows img))

> zipPixel :: (Int -> Int -> Int) -> Pixel -> Pixel -> Pixel
> zipPixel f (aa, ab, ac) (ba, bb, bc) = (aa `f` ba, ab `f` bb, ac `f` bc)

> mapPixel :: (Int -> Int) -> Pixel -> Pixel
> mapPixel f (a, b, c) = (f a, f b, f c)

Image Convolution
*****************

Take only the rows of a sizedRowImage

> rows :: SizedRowImage -> [[Pixel]]
> rows (_, _, pd) = pd

> eachCoordinate :: Int -> Int -> Int -> Int -> [(Int, Int)]
> eachCoordinate a b w h = [(x,y) | y <- [b..h-1], x <- [a..w-1]]

Crop a (w, h) sized image around the center (x, y)

> cropAround :: (Int, Int) -> (Int, Int) -> SizedImage -> SizedImage
> cropAround (x, y) (w, h) img = cropImage (x - div w 2) (y - div h 2) w h img

Convert the image to a SizedImage from a SizedRowImage,
  apply transformation f and convert the result 
  to a SizedRowImage.

> transformAsRows :: (SizedImage -> SizedImage) -> SizedRowImage -> SizedRowImage
> transformAsRows f img = (convertToRows.f.convertFromRows) img

All Zeroes Pixel

> zeroPixel :: Pixel
> zeroPixel = (0, 0, 0)

Multiply the patch[x,y] with convolution[x, y]

> multiplyPatchPixel :: [[Pixel]] -> [[Pixel]] -> (Int, Int) -> Pixel
> multiplyPatchPixel patch kernel (x, y) = zipPixel (*) 
>    (patch !! y !! x)
>    (kernel !! y !! x)

Multiply all the pixels in the patch with their
  corresponding convolution pixels giving a 
  flat array of pixels

> multiplyPatch :: Int -> Int -> [[Pixel]] -> [[Pixel]] -> [Pixel]
> multiplyPatch w h patch kernel = map 
>    (multiplyPatchPixel patch kernel) 
>    (eachCoordinate 0 0 w h)

Sum all the pixels in the patch multiplied with corresponding convolution values.

> --                     Image           Kernel        X      Y     Result
> convolutePixel :: SizedRowImage -> SizedRowImage -> (Int, Int) -> Pixel
> -- Multiply each pixel in the patch by the corresponding convolution matrix pixel 
> convolutePixel img (w, h, kernel) (x, y) = 
> 	foldr (zipPixel (+)) zeroPixel (multiplyPatch w h patch kernel)
>             where (nw, nh, patch) = transformAsRows (cropAround (x, y) (w, h)) img

> --                      Kernel        Source Img         Result
> convoluteRowImage :: SizedRowImage -> SizedRowImage -> SizedImage
> convoluteRowImage kernel (w, h, src) = (w-2, h-2, map
>    (convolutePixel (w, h, src) kernel) 
>    (eachCoordinate 1 1 (w-1) (h-1)))

Convolute the image by a kernel

> convoluteImage :: SizedRowImage -> SizedImage -> SizedImage
> convoluteImage kernel img = convoluteRowImage kernel (convertToRows img)

[Note that for the sample image you will get only 2
  pixels, because of the small size of the image]

> minPixelValue, maxPixelValue :: Pixel -> Int
> maxPixelValue (r,g,b) = maximum [r,g,b]
> minPixelValue (r,g,b) = minimum [r,g,b]

 Normalise Image
*****************

     x - min
  ------------- * max
    max - min

> normaliseValue :: Int -> Int -> Int -> Int
> normaliseValue low high val = 
>    floor ((((fromIntegral val) - (fromIntegral low)) / 
>        ((fromIntegral high) - (fromIntegral low))) * 255.0)

> normalisePixelValue :: Int -> Int -> Pixel -> Pixel
> normalisePixelValue low high pixel = mapPixel (normaliseValue low high) pixel

> maxImageValue, minImageValue :: [Pixel] -> Int
> maxImageValue pixels = (maximum.map (maxPixelValue)) pixels
> minImageValue pixels = (minimum.map (minPixelValue)) pixels

> normaliseImage :: SizedImage -> SizedImage
> normaliseImage (w, h, pd) = (w, h, map (normalisePixelValue minPx maxPx) pd)
>    where
>         minPx = minImageValue pd
>         maxPx = maxImageValue pd

 Convolute then normalise the image
************************************

> convoluteNormalised :: SizedRowImage -> SizedImage -> SizedImage
> convoluteNormalised kernel = (normaliseImage.(convoluteImage kernel))

 Convolute then make an average out of the sums (/9)
*****************************************************

> convoluteAveraged :: SizedRowImage -> SizedImage -> SizedImage
> convoluteAveraged (kw, kh, kernel) image = (w, h, [mapPixel (`div` 9) x | x <- img])
>    where
>         (w, h, img) = convoluteImage (kw, kh, kernel) image
>         kernelArea  = (kw *  kh)


 Stock Convolution Kernels
***************************

> sobelKernel :: SizedRowImage
> sobelKernel = (3, 3, [[(-1, -1, -1), (0, 0, 0), (1, 1, 1)],
>                       [(-2, -2, -2), (0, 0, 0), (2, 2, 2)], 
>                       [(-1, -1, -1), (0, 0, 0), (1, 1, 1)]])

> sobel :: SizedImage -> SizedImage
> sobel = (convoluteNormalised sobelKernel)

> embossKernel :: SizedRowImage
> embossKernel = (3, 3, [[(-2, -2, -2), (-2, -2, -2), (0, 0, 0)], 
>                        [(-2, -2, -2), (6, 6, 6), (0, 0, 0)],
>                        [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])

> emboss :: SizedImage -> SizedImage
> emboss = (convoluteNormalised embossKernel)

> sharpenKernel :: SizedRowImage
> sharpenKernel = (3, 3, [[(0, 0, 0), (-1, -1, -1), (0, 0, 0)], 
>                         [(-1, -1, -1), (5, 5, 5), (-1, -1, -1)], 
>                         [(0, 0, 0), (-1, -1, -1), (0, 0, 0)]])

> sharpen :: SizedImage -> SizedImage
> sharpen = (convoluteNormalised sharpenKernel)

> blurKernel :: SizedRowImage
> blurKernel = (3, 3, [[(1, 1, 1), (1, 1, 1), (1, 1, 1)], 
>                      [(1, 1, 1), (1,1,1), (1, 1, 1)],7
>                      [(1, 1, 1), (1, 1, 1), (1, 1, 1)]])

> blur :: SizedImage -> SizedImage
> blur = (convoluteAveraged blurKernel)

Kernel that does nothing to an image (will effectivley crop out from a 1 pixel border)

> noChangeKernel :: SizedRowImage
> noChangeKernel = (3, 3, [[(0, 0, 0), (0, 0, 0), (0, 0, 0)], 
>                          [(0, 0, 0), (1, 1, 1), (0, 0, 0)], 
>                          [(0, 0, 0), (0, 0, 0), (0, 0, 0)]])
\end{minted}

\centering
\emph{[Please do not attempt to copy and paste from this PDF, \LaTeX has removed lines automatically and replaced it with spacing, added unicode quotes instead of backticks and copying from most readers will include page numbers also. Instead refer to the imageIO.lhs included with this submission]}

\end{document}