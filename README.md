# U08074 Reasoning About Functional Programs Coursework
	
***
## Practicalities

The details of the coursework will be explained in lectures. You should 
take advantage of the practical sessions to obtain **feedback** on your
work at your own pace as you work through the tasks. There is a surgery 
in Week 8, to allow you to work through the tasks 
with some support and more feedback.

**Deadline:** Wednesday 5pm Week 9 (15 April).

**Feedback:** Prompt feedback by Wednesday 5pm Week 11 (29 April).

## Background Information

To make this coursework entertaining (hopefully!) and show you the
sort of uses functional programming can be put to (on a small scale),
the functions you are going to write will manipulate _images_.
If you don't currently know anything about images, that's ok.

You will be using the following datatypes for manipulating images :

```
#!haskell

type Pixel      = (Int,Int,Int)
type PixelData  = [Pixel]
type SizedImage = (Int,Int,PixelData)
```

Thus the __Pixel__ type will be used for RGB values, the pixel
data will simply be represented as a list of pixels, and the
__SizedImage__ type will be used to represent images. Each image
is represented by its width, its height, and its pixel data.

For example, the following image

![demo image](https://bitbucket.org/repo/B885nG/images/3112457851-demo4x3.gif)

would be represented by

```
#!haskell
sampleImage = (4,3, [(0,0,0), (  0,  0,  0), (  0,  0,  0), (255,  0,  0),
   (0,0,0), (255,255,255), (255,  0,  0), (150,150,255),
   (0,0,0), (255,  0,  0), (150,150,255), (150,150,255)])
```

...which is of type __SizedImage__.


## Getting Started

First of all, you will need some small images in PPM format to
practise on. You can either supply your own, or use the ones supplied
with this coursework. You can convert any standard image files to PPM
format using a utility like Gimp, which is installed in the pooled
rooms and also free to download.


To manipulate images in a program, some form of image input and
output is needed. This is supplied for you, in the file imageIO.lhs.
Note that this is a **literate** script, hence the
extension .lhs. You can add your own functions after the supplied
code.

### Preliminary Exercise

To get started, load imageIO.lhs into Haskell (it should load OK) and evaluate the following expression
```
#!haskell
transform noChange
```
you will be prompted for an input filename, so type the name of one of
your files, e.g. __sean.ppm__ and give a different filename (of your
choice) for output (make sure you include the .ppm extensions).

The function __noChange :: SizedImage ->
SizedImage__ is a transformation that you have been supplied
with that does nothing, so if you open the output file you will
see the same image as the input file.

In the exercises that follow you will write several more
transformations of type __SizedImage -> SizedImage__
and test them out in the same way using __transform__. You
**must** include examples of your tests to be
awarded all the marks for every function. You might find it
useful to use the supplied constant __sampleImage ::
SizedImage__ in your tests, or you may wish to supply your
own test case.

Now that you're up and running, the tasks below describe the
functions you should write...

### Section A

Recall that the following datatypes are already defined
for you (and you should not change them) 

```
#!haskell

type SizedImage  = (Int,Int,PixelData)
type PixelData   = [Pixel]
type Pixel       = (Int,Int,Int)

```

Now define the following functions which return the
width, height, and list of pixel values, given an
image:

```
#!haskell

width :: SizedImage -> Int
height :: SizedImage -> Int
pixelData :: SizedImage -> [Pixel]

```

Also define these functions, which given an
individual __(red,green,blue)__ pixel, return the
red value, green value, and blue value
respectively.

```
#!haskell

redValue :: Pixel -> Int
greenValue :: Pixel -> Int
blueValue :: Pixel -> Int

```

Write a function __makeGrey__ of type
```
#!haskell

makeGrey :: Pixel -> Pixel

```

which takes a pixel and assigns to each RGB value
the average of the RGB values. This converts a colour
into a shade of grey. (Note that the average of the
values is unlikely to be a whole number, so you can
just round the average down to an integer, and that
will do fine.)

Use the above function to supply the definition of a
function __greyscale__ with type
```
#!haskell

greyscale :: SizedImage -> SizedImage

```

so that it turns an image from colour into shades of
grey:

![acorns](https://bitbucket.org/repo/B885nG/images/2900011858-acorns.gif)
![goes to](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![greyed acorns](https://bitbucket.org/repo/B885nG/images/577331893-acorns-greyed.gif)

Write a function __invertPixel__ of type

```
#!haskell

invertPixel :: Pixel -> Pixel

```

which takes a pixel and assigns to each RGB value
its "opposite" value, which is __255 -__ the
original (e.g. __0__ is the opposite of
__255__, __253__ is the opposite of
__2__, etc.).

Use the above function to supply the definition of a
function __invert__ with type

```
#!haskell

invert :: SizedImage -> SizedImage

```

so that it turns an image into its negative:

![lantern picture](https://bitbucket.org/repo/B885nG/images/462330988-lantern.gif)
![goes to](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![inverted lantern](https://bitbucket.org/repo/B885nG/images/2279732493-lantern-inverted.gif)

Include a test case that applies your
__invert__ function twice, and check that the
output file is the same as the input, since inverting
the pixels twice should take them back to their
original value.

Write a function __darken__ to darken an image

```
#!haskell

darken :: SizedImage -> SizedImage

```

like this:

![ghost picture](https://bitbucket.org/repo/B885nG/images/3385300106-ghost.gif)
![goes to](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![darker ghost picture](https://bitbucket.org/repo/B885nG/images/2554818346-ghost-darkened.gif)

Be careful to keep your pixel values within the range __0..255__ !

Implement a function __redComponent__ which
displays just the red component of an image

```
#!haskell

redComponent :: SizedImage -> SizedImage

```

like this:

![bonfire picture](https://bitbucket.org/repo/B885nG/images/3381736793-bonfire.jpg)
![goes to](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![red channel of the bonfire picture](https://bitbucket.org/repo/B885nG/images/123573229-bonfire-reddened.gif)

Implement a function __averagePixels__ which
calculates the average brightness of all the pixels in
an image. The brightness of one pixel is the average of
its RGB values.

```
#!haskell

averagePixels :: SizedImage -> Int

```

You will need to use the (already supplied) IO
function __analyse__ for this. For example,
evaluating the expression __analyse
averagePixels__ on this image

![diana rigg picture](https://bitbucket.org/repo/B885nG/images/733441474-diana-rigg.jpg)

results in an average pixel brightness of __76__.

Implement a function __threshold__ which sends
all the pixels brighter than a given threshold to
white, and all the pixels darker than a given threshold
to black.

```
#!haskell

threshold :: Int -> SizedImage -> SizedImage

```

For example, this is the result of doing
__transform (threshold 120)__

![diana rigg picture](https://bitbucket.org/repo/B885nG/images/733441474-diana-rigg.jpg)
![goes to](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![threshold 120](https://bitbucket.org/repo/B885nG/images/592862517-diana-rigg-t120.gif)

And this is the result of doing __transform
(threshold 180)__

![diana rigg picture](https://bitbucket.org/repo/B885nG/images/733441474-diana-rigg.jpg)
![goes to](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![threshold 180](https://bitbucket.org/repo/B885nG/images/2252615755-diana-rigg-t180.gif)

Define a function __makeSplit__ which splits up
a list into its first __n__ elements (or fewer if
there are less than n elements in the list) and the
rest of the elements.
```
#!haskell

makeSplit :: Int -> [a] -> ([a],[a])

```

For example,
```
#!haskell

makeSplit 5 [1..14]  => ([1,2,3,4,5],[6,7,8,9,10,11,12,13,14])

```

Now use the __makeSplit__ function to define a
__getRows__ function which transforms a list into
a list of sublists of length __w__ (the last list
might have to be shorter).
```
#!haskell

getRows :: Int -> [a] -> [[a]]

```

For example,
```
#!haskell

getRows 5 [1..14]  => [[1,2,3,4,5],[6,7,8,9,10],[11,12,13,14]]

```

Sometimes it is more convenient to represent the
pixel data in a list of rows of pixels, rather than in
one long list. So, define a new type
```
#!haskell

type SizedRowImage = (Int,Int,[[Pixel]])

```

Then implement two conversion functions to convert
back and forth between a __SizedImage__ and its
__SizedRowImage__ equivalent:
```
#!haskell

convertToRows :: SizedImage -> SizedRowImage
convertFromRows :: SizedRowImage -> SizedImage

```

So, for example, taking the data from our sample
image,
```
#!haskell

convertToRows (4,3, [(0,0,0), (  0,  0,  0), (  0,  0,  0), (255,  0,  0),
				   (0,0,0), (255,255,255), (255,  0,  0), (150,150,255),
				   (0,0,0), (255,  0,  0), (150,150,255), (150,150,255)])
=>
(4,3, [[(0,0,0), (0,0,0), (0,0,0), (255,0,0)],
	 [(0,0,0), (255,255,255), (255,0,0), (150,150,255)],
	 [(0,0,0), (255,0,0), (150,150,255), (150,150,255)]])

```

and
```
#!haskell

convertFromRows (4,3, [[(0,0,0), (0,0,0), (0,0,0), (255,0,0)],
					 [(0,0,0), (255,255,255), (255,0,0), (150,150,255)],
					 [(0,0,0), (255,0,0), (150,150,255), (150,150,255)]])
=>
(4,3, [(0,0,0), (  0,  0,  0), (  0,  0,  0), (255,  0,  0),
	 (0,0,0), (255,255,255), (255,  0,  0), (150,150,255),
	 (0,0,0), (255,  0,  0), (150,150,255), (150,150,255)])

```

Now implement the following functions (the last two
are particularly challenging!):

```
#!haskell

upsideDown :: SizedImage -> SizedImage
flipVertically :: SizedImage -> SizedImage
flipHorizontally :: SizedImage -> SizedImage
rotateLeft :: SizedImage -> SizedImage
rotateRight :: SizedImage -> SizedImage

```

which, as you might imagine, should have the
following effects:

![Sean Connery picture](https://bitbucket.org/repo/B885nG/images/3794851499-sean-connery.jpg)
upsideDown ![upsideDown](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![sean-ud.jpg](https://bitbucket.org/repo/B885nG/images/2487904952-sean-ud.jpg)

![Sean Connery picture](https://bitbucket.org/repo/B885nG/images/3794851499-sean-connery.jpg)
flipVertically  ![flipVertically](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![sean-fv.jpg](https://bitbucket.org/repo/B885nG/images/3356174670-sean-fv.jpg)

![Sean Connery picture](https://bitbucket.org/repo/B885nG/images/3794851499-sean-connery.jpg)
flipHorizontally  ![flipHorizontally](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![sean-fh.jpg](https://bitbucket.org/repo/B885nG/images/3574922287-sean-fh.jpg)

![Sean Connery picture](https://bitbucket.org/repo/B885nG/images/3794851499-sean-connery.jpg)
rotateLeft  ![rotateLeft](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![sean-rl.jpg](https://bitbucket.org/repo/B885nG/images/893287512-sean-rl.jpg)

![Sean Connery picture](https://bitbucket.org/repo/B885nG/images/3794851499-sean-connery.jpg)
rotateRight  ![rotateRight](https://bitbucket.org/repo/B885nG/images/731948577-arrow.gif)
![sean-rr.jpg](https://bitbucket.org/repo/B885nG/images/2259235419-sean-rr.jpg)

_Hint: You may find that your answers to the previous question are useful._

## Section B

This section is your chance to show us what you can do!

Implement **between two and four functions** that
transform images in an interesting way. You could try one of
these suggestions:


>	Increase the brightness; reduce or increase the contrast;
>	enlarge the image; reduce the image in size; produce some
>	statistics about how many of which colour pixel there are
>	in the image; stretch the image horizontally or
>	vertically...


Alternatively you can implement your own ideas for image
transformations.

## Hints and Marking Information

1. Marks for Section A: 80%
1. Marks for Section B: 20%

Most of the marks will be awarded for correctness and good
commenting of program code. There will be a small number of
marks awarded for elegant code: short function definitions, and
good use of high-order functions (like __map__,
__filter__, __fold__, __concat__ and others
you've met in your practicals).If you have any further
questions, please email [cemartin@brookes.ac.uk](mailto:cemartin@brookes.ac.uk "Clare Martin")